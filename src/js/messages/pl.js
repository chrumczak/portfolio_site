module.exports = {
  'home.about-header': 'O mnie',
  'home.technologies-header': 'Technologie',
  'home.projects-header': 'Projekty',
  'home.contact-header': 'Kontakt',

  'home.source-code-link': 'Kod źródłowy',
  'home.site-link': 'Demo',

  'home.used-stuff': 'Użyte technologie',
  'home.technologies-asterisk': 'Kropki to wypadkowa moich umiejętności i chęci do pracy w danej technologii 🐻',
};
