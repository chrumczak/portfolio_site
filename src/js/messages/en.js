module.exports = {
  'home.about-header': 'About',
  'home.technologies-header': 'Technologies',
  'home.projects-header': 'Projects',
  'home.contact-header': 'Contact',

  'home.source-code-link': 'Source code',
  'home.site-link': 'Demo',

  'home.used-stuff': 'Stack',
};
