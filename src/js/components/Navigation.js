import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Header from 'grommet/components/Header';

import Tiles from 'grommet/components/Tiles';
import Tile from 'grommet/components/Tile';

import { FormattedMessage } from 'react-intl';

import Logo from './Logo';
import NavigationElement from './NavigationElement';
import styled from 'styled-components';

import TechnologyIcon from 'grommet/components/icons/base/Technology';
import MultipleIcon from 'grommet/components/icons/base/Multiple';
import MailIcon from 'grommet/components/icons/base/Mail';
import UserIcon from 'grommet/components/icons/base/User';

import * as Scroll from 'react-scroll';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
    Scroll.animateScroll.scrollMore(1);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    this.forceUpdate();
  }

  render() {
    const {
      isMobile,
      className
    } = this.props;

    return (<React.Fragment>
      {isMobile ?
        <Box align='center' style={{ padding: '12px', position: 'absolute', right: 0 }}>
          <Logo
            isMobile={isMobile}
          />
        </Box> : null
      }

      <StyledHeader
        className={`zgrommetux-background-color-index--dark ${className}`}
        mobile={isMobile ? 1 : 0}
        pad={{ horizontal: isMobile ? 'none' : 'medium' }}
        onTop={(window.scrollY < 20) ? 1 : 0}
      >
        {isMobile ? null :
          <Box margin={{ horizontal: 'small' }}>
          <Logo isMobile={isMobile} />
        </Box>
        }

        <Spacer />

        <Tiles
          responsive={false}
          direction='row'
          justify='end'
        >
          {[<NavigationElement
            name='about'
            label={<FormattedMessage id='home.about-header' />}
            icon={<UserIcon />}
            isMobile={isMobile}
          />,
            <NavigationElement
            name='technologies'
            label={<FormattedMessage id='home.technologies-header' />}
            icon={<TechnologyIcon />}
            isMobile={isMobile}
          />,
            <NavigationElement
            name='projects'
            label={<FormattedMessage id='home.projects-header' />}
            icon={<MultipleIcon />}
            isMobile={isMobile}
          />,
            <NavigationElement
            name='contact'
            label={<FormattedMessage id='home.contact-header' />}
            icon={<MailIcon />}
            isMobile={isMobile}
          />].map((x, i) => (<Tile
            key={`nav-${i}`}
            pad={isMobile ? { horizontal: 'medium' } : null}
            basis={isMobile ? '1/4' : null}
            responsive={!isMobile}
          >{x}</Tile>))}
        </Tiles>
      </StyledHeader>
    </React.Fragment>);
  }
}

const StyledHeader = styled(({ mobile, onTop, ...rest }) => <Header {...rest} />)`
  position: fixed;
  z-index: 999;

  background: rgba(255, 255, 255, 0) !important;
  transition: all .6s ;

  border-radius: 10px;

  ${({ mobile, onTop }) => (
    mobile ?
      `
      bottom: 4px;
      width: calc( 100% - 8px );
      margin: 0 4px;
      background: rgba(255, 255, 255, .96) !important;
      box-shadow: 0 -4px 12px 0 rgba(0, 0, 0, .05);
      `
      :
      `
        top: 0;

        ${onTop ? `        
          width: 100%;
          margin: 0;
          border-radius: 0;
          padding: 0 8px;     
        ` : `
          width: calc( 100% - 16px );
          margin: 4px 8px 0 8px;  
          padding: 0;
         
          background: rgba(255, 255, 255, .96) !important;
          box-shadow: 0 4px 12px 0 rgba(0, 0, 0, .02);       
        `}

    `)}

`;

const Spacer = styled.div`
  flex-grow: 1;
`;

export default Navigation;
