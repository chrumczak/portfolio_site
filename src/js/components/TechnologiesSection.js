import React, { Component } from 'react';
import Tiles from 'grommet/components/Tiles';
import TechnologiesSectionElement from './TechnologiesSectionElement';


class TechnologiesSection extends Component {
  render() {
    const { items, isMobile } = this.props;

    items.sort((a, b) => b.experience - a.experience);

    const itemsToRender = items.map((item, index) => (<TechnologiesSectionElement
      key={`technologies-${index}`}
      item={item}
      index={index}
      isMobile={isMobile}
    />));

    return (<Tiles
      justify='center'
      fill={false}
      responsive={false}>
      {itemsToRender}
    </Tiles>);
  }
}

export default TechnologiesSection;
