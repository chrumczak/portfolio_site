import React, { Component } from 'react';
import { THEME } from '../config';
import ScrollAnimation from 'react-animate-on-scroll';

import Section from 'grommet/components/Section';
import Headline from 'grommet/components/Headline';
import Box from 'grommet/components/Box';
import * as Scroll from 'react-scroll';
import styled, { ThemeProvider } from 'styled-components';

const BACKGROUND_GRADIENT = `linear-gradient(140deg, ${THEME.brand} 40%, ${THEME.brandVariant})`;

class SectionWrapper extends Component {
  render() {
    const {
      children,
      name,
      scrollOffset,
      heading,
      gradientBg,
      style,
      addScrollAreaTop,
      customBackgroundImg
    } = this.props;

    const outerStyle = Object.assign({}, { width: '100%' }, style);

    const innerStyle = Object.assign({}, {
      marginTop: 0 },
    { background: ` ${customBackgroundImg}, ${BACKGROUND_GRADIENT}` }
    );

    return (
      <Scroll.Element
        name={name}
        offset={scrollOffset || 0}
        style={{ marginTop: -addScrollAreaTop || 0, paddingTop: addScrollAreaTop || 0 }}
      >
        <ThemeProvider theme={THEME}>

          <Section
            style={outerStyle}
            pad='none'
            className={gradientBg ?
              'grommetux-background-color-index--dark' :
              'grommetux-background-color-index-light-22'
            }
          >
            <BoxWithGradient
              gradient={gradientBg ? 1 : 0}
              margin='medium'
              style={innerStyle}
            >

              <Box
                pad={{ vertical: 'large', horizontal: 'medium' }}
                align='center'
              >
                { heading ?
                  <ScrollAnimation
                    style={{ width: '100%' }}
                    animateOnce={true}
                    animateIn='fadeInUp'
                    delay={100}
                  >
                    <StyledHeadline
                      strong={true}
                      align='center'
                      size='large'
                      margin='large'
                      style={{ marginTop: 0 }}
                    >
                      {heading}
                    </StyledHeadline>
                  </ScrollAnimation> :
                  null
                }

                { children }
              </Box>
            </BoxWithGradient>
          </Section>
        </ThemeProvider>
      </Scroll.Element>

    );
  }
}

export default SectionWrapper;


const StyledHeadline = styled(Headline)`
  @media screen and (max-width: 480px) {
    font-size: 2.5rem;
  }
`;

const BoxWithGradient = styled(Box)`  
  ${props => (props.gradient ?
    `background: ${BACKGROUND_GRADIENT};` :
    '')
}
`;

