import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import App from 'grommet/components/App';
import Responsive from 'grommet/utils/Responsive';
import { Tooltip } from 'react-lightweight-tooltip';

import TechnologiesSection from './TechnologiesSection';
import ProjectsSection from './ProjectsSection';
import Navigation from './Navigation';
import AboutSection from './AboutSection';
import ContactSection from './ContactSection';
import SectionWrapper from './SectionWrapper';
import FleeingSquare from './FleeingSquare';

import { FormattedMessage, injectIntl } from 'react-intl';
import { THEME, MOBILE_NAVIGATION_HEIGHT, DESKTOP_NAVIGATION_HEIGHT } from '../config';

import styled, { ThemeProvider } from 'styled-components';
import data from '../../data/data.js';


// ICONS - http://konpa.github.io/devicon/
class Main extends Component {
  constructor() {
    super();
    this._onResponsive = this._onResponsive.bind(this);
    this.state = {};
  }

  componentWillMount() {
    this._responsive = Responsive.start(this._onResponsive);
  }

  componentWillUnmount() {
    this._responsive.stop();
  }

  _onResponsive(isMobile) {
    this.setState({ isMobile });
  }

  render() {
    const isMobile = this.state.isMobile;
    const { locale } = this.props.intl;

    const technologies = data[locale].TECHNOLOGIES;
    const projects = data[locale].PROJECTS;
    const contact = data[locale].CONTACT;
    const info = data[locale].INFORMATIONS;

    const TooltipStyles = {
      content: {
        color: 'white',
        backgroundColor: 'transparent',

      },
      tooltip: {
        zIndex: '1000',
        backgroundColor: '#435363',
        borderRadius: '0px',
        fontWeight: '300',
        fontSize: '20px',
        lineHeight: '1.45',
        width: '40vw',
      },
      arrow: {
        borderTop: '5px solid #435363',
      },
    };

    const TechnologiesAsterisk = (
      <Tooltip
        key={'TechnologiesAsteriskTooltip'}
        content={this.props.intl.formatMessage({ id: 'home.technologies-asterisk' })}
        styles={TooltipStyles}
      >
        <span
          style={{
            cursor: 'pointer',
            fontSize: '50%',
            verticalAlign: 'top',
            color: 'rgba(255, 255, 255, .3)'
          }}>*</span>
      </Tooltip>
    );

    return (
      <StyledApp
        mobile={isMobile ? 1 : 0}
        centered={false}
        height={window.scrollY}
      >
        <ThemeProvider theme={THEME}>
          <Navigation
            isMobile={isMobile}
            navigationHeight={96}
          />
        </ThemeProvider>

        <FleeingSquare />

        <SectionWrapper
          name='about'
          isMobile={isMobile}
          addScrollAreaTop={200}
          style={{ marginTop: '24px' }}
        >
          <AboutSection
            isMobile={isMobile}
            text={info.text}
            imageSrc={info.imageSrc}
          />
        </SectionWrapper>

        <SectionWrapper
          isMobile={isMobile}
          heading={
            [<FormattedMessage
              id='home.technologies-header'
              key={'TechnologiesHeading'} />,
            TechnologiesAsterisk]
          }
          name='technologies'
          gradientBg
        >
          <Box pad={{ horizontal: isMobile ? 'small' : 'large' }}>
            <TechnologiesSection
              items={technologies}
              isMobile={isMobile}
            />
          </Box>
        </SectionWrapper>

        <SectionWrapper
          isMobile={isMobile}
          heading={<FormattedMessage id='home.projects-header' />}
          addScrollAreaTop={-40}
          name='projects'
        >
          <Box pad={{ horizontal: isMobile ? 'small' : 'large' }}>
            <ProjectsSection
              items={projects}
              isMobile={isMobile}
            />
          </Box>
        </SectionWrapper>

        <SectionWrapper
          isMobile={isMobile}
          heading={<FormattedMessage id='home.contact-header' />}
          name='contact'
          gradientBg
          customBackgroundImg={'url(\'img/bottom-bg.svg\') no-repeat left bottom/50% '}
        >
          <ContactSection
            name={contact.name}
            email={contact.email}
            isMobile={isMobile}
          />
        </SectionWrapper>
      </StyledApp>
    );
  }
}
export default injectIntl(Main);

const StyledApp = styled(App)`
  background: url('img/top-bg.svg') no-repeat right top, // some squares on right top
  url('img/bg.svg') no-repeat left 50%; // squares on half site height
  background-size: 50%;

  ${props => (props.mobile ?
    `padding-bottom: ${MOBILE_NAVIGATION_HEIGHT}px;`
    :
    `padding-top: ${DESKTOP_NAVIGATION_HEIGHT}px;`
  )}
`;

