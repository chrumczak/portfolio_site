import React, { Component } from 'react';
import Title from 'grommet/components/Title';
import Image from 'grommet/components/Image';

import styled from 'styled-components';

class Logo extends Component {
  render() {
    const { isMobile } = this.props;

    return (<StyledTitle
      responsive={false}
      mobile={isMobile ? 1 : 0}
    >

      <StyledImage
        src='/img/logo.png'
      />

      <MobileLogoText>
          eś
      </MobileLogoText>

      <DesktopLogoText>
          Eliasz Śliż
      </DesktopLogoText>

    </StyledTitle>);
  }
}

export default Logo;

const SMALLER_LOGO_BREAKPOINT = 800;

const MobileLogoText = styled(Title)`
  position: absolute;
  left: 30px;
  font-weight: 800;
  font-size: 2rem;
  
  @media screen and (min-width: ${SMALLER_LOGO_BREAKPOINT + 1}px) {
    display: none;
  }
`;

const DesktopLogoText = styled(Title)`
  position: absolute;
  left: 20px;
  font-weight: 400;
  font-size: 1.3rem;
  
  @media screen and (max-width: ${SMALLER_LOGO_BREAKPOINT}px) {
    display: none;
  }
`;


const StyledImage = styled(Image)`
  height: 50px;
  width: auto; 
`;

const StyledTitle = styled(Title)`
  position: relative;
  width: 80px;
    
  @media screen and (min-width: ${SMALLER_LOGO_BREAKPOINT + 1}px) {
    width: 150px;
  }
`;
