import React, { Component } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import Paragraph from 'grommet/components/Paragraph';

import Tile from 'grommet/components/Tile';
import Box from 'grommet/components/Box';

import Image from 'grommet/components/Image';
import Card from 'grommet/components/Card';
import Heading from 'grommet/components/Heading';

import RadialIcon from 'grommet/components/icons/base/Radial';
import styled from 'styled-components';

const CARD_SMALL_WIDTH = 192;

class TechnologiesSectionElement extends Component {
  render() {
    const { item, isMobile, index } = this.props;

    const thumbnailItem = (<Image
      src={item.thumbnailUrl}
      style={{ height: '80px', width: 'auto' }}
    />);


    const heading = (<Heading
      align='center'
      style={item.name && item.name.length > 6 ? { lineHeight: 'calc(1.23 * 2.25rem)', fontSize: '1.8rem' } : null}
      tag='h2'
      margin='small'
    >
      {item.name}
    </Heading>);

    const experienceIcons = [];
    for (let i = 0; i < 3; i += 1) {
      experienceIcons.push(<RadialIcon
        size='xsmall'
        key={i}
        style={{ margin: '0 2px' }}
        className={item.experience > i ? 'fill-brand-gradient' : ''}
      />);
    }

    const isBroad = !!(item.description);

    return (
      <StyledTile
        responsive={false}
        mobile={isMobile ? 1 : 0}
        wide={(isBroad && isMobile)}
        pad='small'
      >
        <ScrollAnimation
          style={{ width: '100%' }}
          animateOnce={true}
          animateIn='fadeInUp'
          delay={100 * index}
        >
          <StyledCard
            colorIndex='light-2'
            margin='none'
            contentPad='none'
            broad={isBroad ? 1 : 0}
            mobile={isMobile ? 1 : 0}
            responsive={false}
          >
            <Box
              direction={(isBroad && isMobile) ? 'column' : 'row'}
              justify='center'
              responsive={false}
            >
              <FirstSection
                broad={isBroad ? 1 : 0}
                responsive={false}
              >
                <Box align='center'>
                  { thumbnailItem }
                </Box>

                { heading }

                <Box direction='row' justify='center' responsive={false}>
                  { experienceIcons }
                </Box>
              </FirstSection>

              {
                isBroad ?
                  <SecondSection
                    margin={{ top: (isBroad && isMobile) ? 'medium' : 'none' }}
                  >
                    <Paragraph margin='none'>
                      { item.description }
                    </Paragraph>
                  </SecondSection>
                  : null
              }
            </Box>
          </StyledCard>
        </ScrollAnimation>
      </StyledTile>
    );
  }
}

export default TechnologiesSectionElement;

const FirstSection = styled(Box)`
  ${({ broad }) => (broad ? `
    flex: 0 152px;
  ` : null)}
`;

const SecondSection = styled(Box)`
  flex: 1;
`;

const StyledTile = styled(Tile)`  
  flex-basis: ${(props) => {
    if (props.mobile) {
      return props.broad ? '100%' : '50%';
    }
    return 'auto';
  }};
  
  @media screen and (max-device-width: 430px) {
    flex-basis: 100%;
  }
`;

const StyledCard = styled(Card)`
  box-shadow: 5px 0 16px 0 rgba(0,0,0,.02);
  width: ${(props) => {
    if (props.mobile) return '100%;';

    let cardWidth = props.broad ? (CARD_SMALL_WIDTH * 2) : CARD_SMALL_WIDTH;

    if (props.broad) {
      cardWidth += 24;
    }
    return `${cardWidth}px;`;
  }}
  
  padding: 24px;
`;

