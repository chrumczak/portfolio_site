import React, { Component } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';
import Tile from 'grommet/components/Tile';
import Tiles from 'grommet/components/Tiles';
import ProjectsSectionElement from './ProjectsSectionElement';
import styled from 'styled-components';

class ProjectsSection extends Component {
  render() {
    const { items, isMobile } = this.props;

    const tilesToRender = items.map((item, index) => (
      <StyledTile
        key={`project-${index}`}
        pad='small'
      >
        <ScrollAnimation
          animateIn='fadeInUp'
          animateOnce={true}
        >
          <ProjectsSectionElement
            item={item}
            isMobile={isMobile}
            key={item.name}
          />
        </ScrollAnimation>
      </StyledTile>

    ));

    return (<Tiles>
      { tilesToRender }
    </Tiles>);
  }
}

export default ProjectsSection;

const StyledTile = styled(Tile)`
  @media screen and (min-device-width: 1000px) {
    flex-basis: 50%;
  }
`;
