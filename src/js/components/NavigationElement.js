import React, { Component } from 'react';
import Button from 'grommet/components/Button';
import Box from 'grommet/components/Box';

import styled from 'styled-components';

import * as Scroll from 'react-scroll';

class NavigationElement extends Component {
  render() {
    const {
      isMobile,
      name,
      label,
      icon
    } = this.props;

    return (<StyledLinkWrapper
      activeClass='active'
      mobile={isMobile ? 1 : 0}
      to={name}
      spy={true}
      smooth='easeOutCirc'
      duration={500}
      offset={-50}
    >
      {isMobile ?
        <StyledButton
          mobile={isMobile ? 1 : 0}
          plain={true}
          onClick={() => {}}
        >
          <Box
            align='center'
            direction='column'
            margin={{ top: 'small' }}
          >
            {icon}
          </Box>

          {label}
        </StyledButton> :
          <Button
            label={label}
            onClick={() => {}}
            style={{ paddingLeft: '12px', paddingRight: '12px' }}
            plain={true}
          />
      }
    </StyledLinkWrapper>);
  }
}


const StyledLinkWrapper = styled(Scroll.Link)`
  ${({ mobile }) => (mobile ? `  
    & button {
      font-size: .95rem;
      transition: all .5s;
      
      .grommetux-control-icon {
        transition: all .5s;
       }
      
      span {        
        white-space: nowrap;
      }
    }
    
    &.active .grommetux-control-icon {
      transform: scale(1.1);
    }
  ` : ` 
    & button {
     text-transform: uppercase;
     font-size: 1.1rem;
     letter-spacing: .04rem;
          
     &::first-letter {
      font-size: 1.2rem;
     }
     
    }
    
    &.active button {
      font-weight: bold;
    }
  `)}
  
  &.active {
    & button {
      color: ${props => `${props.theme.brand}`} !important;
      
      .grommetux-control-icon {
        fill: ${props => `${props.theme.brand}`};
        stroke: ${props => `${props.theme.brand}`};
        transition: all .5s;
      }
    }
  }
  
`;

const StyledButton = styled(Button)`
`;

export default NavigationElement;
