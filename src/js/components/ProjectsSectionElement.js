import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import Anchor from 'grommet/components/Anchor';
import Tiles from 'grommet/components/Tiles';
import Tile from 'grommet/components/Tile';
import Label from 'grommet/components/Label';
import Heading from 'grommet/components/Heading';
import Card from 'grommet/components/Card';
import { FormattedMessage } from 'react-intl';
import Markdown from 'grommet/components/Markdown';
import Lightbox from 'react-image-lightbox';

import CodeIcon from 'grommet/components/icons/base/Code';
import WebSiteIcon from 'grommet/components/icons/base/Language';

import ImageIcon from 'grommet/components/icons/base/Image';
import styled from 'styled-components';

class ProjectsSectionElement extends Component {
  constructor() {
    super();
    this.state = {
      isOpen: false,
      photoIndex: 0
    };
    this.showLightbox = this.showLightbox.bind(this);
  }

  showLightbox() {
    this.setState({ isOpen: true });
  }

  render() {
    const {
      item,
      isMobile
    } = this.props;

    const {
      photoIndex,
      isOpen
    } = this.state;

    const marginNone = {
      props: {
        margin: 'none'
      }
    };

    const images = item.images || [];

    const thumbnail = (
      item.images && item.images.length > 0 ?
        (<ThumbnailWrapper onClick={this.showLightbox}>
          <Thumbnail
            src={item.thumbnailUrl}
            full='horizontal'
          />

          <ShowPhotosLabel uppercase={true}>
            <ImageIcon
              size='medium'
              style={{ marginRight: '8px' }}
            />
            <span>Więcej zdjęć</span>
          </ShowPhotosLabel>
        </ThumbnailWrapper>) :
        (<Thumbnail
          src={item.thumbnailUrl}
          full='horizontal'
        />)
    );

    const heading = (<Heading
      tag='h3'
      strong={true}
      style={{ marginTop: '24px', cursor: 'pointer' }}
    >{item.title}</Heading>);

    const description = (<Box margin={{ bottom: 'medium' }}>
      <Markdown
        components={{ p: marginNone }}
        content={item.description}
      />
    </Box>);

    const stackDescription = (item.stackDescription ? (<Box margin={{ top: 'medium' }}>
      <Markdown
        components={{ p: marginNone }}
        content={item.stackDescription}
      />
    </Box>) : null);

    const stack = (<Tiles justify='center' responsive={false}>
      {
        item.stack.map(technology => (
          <Tile
            key={technology.name}
            pad='small'
            align={isMobile ? 'center' : 'start'}
            style={{ flexShrink: 0 }}>
            <Image src={technology.iconUrl}
              style={{ height: 'auto', width: '60px' }} />
          </Tile>
        ))}
    </Tiles>);

    const links = [];

    if (item.sourceUrl) {
      links.push(<Anchor
        icon={<CodeIcon />}
        label={<FormattedMessage id='home.source-code-link' />}
        primary={true}
        href={item.sourceUrl}
      />);
    }

    if (item.demoUrl) {
      links.push(<Anchor
        icon={<WebSiteIcon />}
        label={<FormattedMessage id='home.site-link' />}
        primary={true}
        href={item.demoUrl}
      />);
    }


    return (
      <Card
        colorIndex='light-1'
        pad='none'
        contentPad='none'
        style={{
          boxShadow: '5px 0 16px 0 rgba(0, 0, 0, .02)',
          width: isMobile ? 'calc( 100vw - 24px )' : '100%'
        }}
      >
        <CardContent>
          <Box
            margin={{ bottom: 'medium' }}
            onClick={this._onMoreClick}
          >
            {thumbnail}
          </Box>

          {isOpen && (
            <Lightbox
              imagePadding={30}
              mainSrc={images[photoIndex]}
              nextSrc={images[(photoIndex + 1) % images.length]}
              prevSrc={images[(photoIndex + images.length - 1) % images.length]}
              onCloseRequest={() => this.setState({ isOpen: false })}
              onMovePrevRequest={() =>
                this.setState({
                  photoIndex: (photoIndex + images.length - 1) % images.length,
                })
              }
              onMoveNextRequest={() =>
                this.setState({
                  photoIndex: (photoIndex + 1) % images.length,
                })
              }
            />
          )}

          {heading}

          {description}

          {stack}

          {stackDescription}

          <Box margin={{ top: 'medium' }}>
            <Tiles>
              {
                links.map((anchorElement, linkIndex) => (<Tile
                  key={`${linkIndex}-link`}
                  basis='1/2'
                  align={isMobile ? 'start' : 'center'}
                  pad={{ vertical: 'medium' }}
                >
                  {anchorElement}
                </Tile>))
              }
            </Tiles>
          </Box>
        </CardContent>
      </Card>
    );
  }
}

export default ProjectsSectionElement;

const CardContent = styled(Box)`
  padding: 24px;
`;

const ThumbnailWrapper = styled(Box)`
  position: relative;
  cursor: pointer;

  & > .grommetux-label {
    position: absolute;
    top: 50%;
    left: 50%;

    opacity: 0;
    background: rgba(255, 255, 255, .5);
    padding: 10px 20px;

    transform: translate(-50%, -50%) scale(0);
    transition: all 0.1s ease-in 0.1s;

    font-weight: bold;
  }

  &:hover, &:active {
	  & > img {
	    -webkit-filter: blur(1px);
      filter: blur(1px);
      -webkit-transition: .2s ease-in;
      transition: .2s ease-in;

	  }

    & > .grommetux-label {
      opacity: 1;
      transform: translate(-50%, -50%) scale(1);
	    transition-delay: 0.1s;
    }
  }
`;

const ShowPhotosLabel = styled(Label)`
  user-select: none;
  display: flex;
  align-items: center;
  white-space: pre;
`;

const Thumbnail = styled(Image)`
  margin-top: 0;
`;

