import React, { Component } from 'react';
import ScrollAnimation from 'react-animate-on-scroll';

import Box from 'grommet/components/Box';
import Heading from 'grommet/components/Heading';
import Anchor from 'grommet/components/Anchor';
import MailIcon from 'grommet/components/icons/base/Mail';

class ContactSection extends Component {
  render() {
    const { name, email, isMobile } = this.props;

    return (<Box
      align='center'
      pad={{ vertical: 'xlarge' }}
      style={{ paddingTop: '20vh', paddingBottom: '20vh' }}
    >

      <ScrollAnimation animateIn='fadeIn' animateOnce={true}>
        <Heading tag='h2' align='center'>{ name }</Heading>

        <ScrollAnimation
          animateIn='bounceIn'
          delay={300}
          offset={0}
          animateOnce={true}
        >
          <Box
            align='center'
            justify='center'
            direction='row'
            margin={{ horizontal: 'medium', vertical: 'large' }}
          >
            <Anchor href={`mailto:${email}`}>
              <MailIcon size='large' style={{ marginRight: '8px', marginBottom: isMobile ? '4px' : 0 }} />
            </Anchor>

            <Heading tag='h4' margin='none' align='center'>
              <Anchor href={`mailto:${email}`}>
                { email }
              </Anchor>
            </Heading>
          </Box>
        </ScrollAnimation>
      </ScrollAnimation>

      <Box margin={{ bottom: 'large' }} />
    </Box>);
  }
}

export default ContactSection;
