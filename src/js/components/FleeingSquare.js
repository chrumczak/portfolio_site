import React, { Component } from 'react';
import styled from 'styled-components';


class FleeingSquare extends Component {
  constructor() {
    super();
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentWillMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    this.forceUpdate();
  }

  render() {
    return (<StyledDiv height={window.scrollY} />);
  }
}

export default FleeingSquare;

const StyledDiv = styled.div`
  position: absolute;
  top: ${props => (props.height > 0 ? `calc(-5vw - ${props.height * 2}px)` : '-5vw')};
  height: 20vw;
  background-color: rgba(117, 156, 228, .04);
  width: 20vw;
  transform: rotate(45deg);
`;
