import React, { Component } from 'react';
import Box from 'grommet/components/Box';
import Image from 'grommet/components/Image';
import ScrollAnimation from 'react-animate-on-scroll';
import Markdown from 'grommet/components/Markdown';
import Anchor from 'grommet/components/Anchor';
import Paragraph from 'grommet/components/Paragraph';
import MailIcon from 'grommet/components/icons/base/Mail';
import * as Scroll from 'react-scroll';
import styled from 'styled-components';

class AboutSection extends Component {
  constructor(props) {
    super(props);
    this.scrollToContactSection = this.scrollToContactSection.bind(this);
  }

  scrollToContactSection() {
    Scroll.scroller.scrollTo('contact', {
      duration: 500,
      delay: 100,
      smooth: true
    });
  }

  render() {
    const {
      isMobile,
      imageSrc,
      text
    } = this.props;

    const marginNone = {
      props: {
        margin: 'none'
      }
    };

    return (<StyledWrapper
      direction='row'
      alignContent='center'
      responsive={false}
      mobile={isMobile ? 1 : 0}
    >
      <ScrollAnimation
        animateIn='fadeInUp'
        animateOnce={true}
        style={{ display: 'flex', alignItems: 'center', zIndex: 998 }}
      >
        <Avatar
          src={imageSrc}
          size='small'
        />
      </ScrollAnimation>

      <ScrollAnimation
        animateIn='fadeInUp'
        animateOnce={true}
        style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
        delay={200}
      >
        <Box
          colorIndex='light-1'
          style={{
            height: '100%'
          }}
          justify='center'
          pad='large'
          alignSelf='center'
        >

          <ParagraphWrapper
            size='large'
          >

            <Markdown
              content={text}
              components={{ p: marginNone }}
            />

            <Paragraph
              style={{ marginTop: '8px', marginBottom: 0 }}
            >
            Jeśli to właśnie mnie szukasz zapraszam do
              <NoIconAnchor
                animateIcon={false}
                primary={true}
                label='kontaktu'
                style={{ marginLeft: '5px' }}
                onClick={() => { this.scrollToContactSection(); }}
              />.
            </Paragraph>


          </ParagraphWrapper>

        </Box>
      </ScrollAnimation>

    </StyledWrapper>);
  }
}

export default AboutSection;

const MOBILE_AVATAR_BREAKPOINT = 900;

const StyledWrapper = styled(Box)`
  // align-items: center;
  flex-direction: row;
    
  @media screen and (max-width: ${MOBILE_AVATAR_BREAKPOINT}px) {
    width: calc(100vw - 24px);
    flex-direction: column;
  }  
`;

const NoIconAnchor = styled(Anchor)`
 & svg {
  display: none;
 }
`;

const ParagraphWrapper = styled(Box)`
  @media screen and (max-width: ${MOBILE_AVATAR_BREAKPOINT}px) {
    margin-top: 24px;
    width: 100%;
  }
  
  @media screen and (min-width: ${MOBILE_AVATAR_BREAKPOINT + 1}px) {
    margin-left: 24px;
  }
`;

const Avatar = styled(Image)`
  position: relative;
  box-shadow: 2px 0 16px 0 rgba(0, 0, 0, .05);
  
  @media screen and (max-width: ${MOBILE_AVATAR_BREAKPOINT}px) {
    top: 24px;
    left: 15%; // 24px;
  
  }
  
  @media screen and (min-width: ${MOBILE_AVATAR_BREAKPOINT + 1}px) {
    left: 48px; 
    min-width: 180px;
  }
 
}
`;
