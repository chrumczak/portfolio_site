import React, { Component } from 'react';
import { IntlProvider, addLocaleData } from 'react-intl';
import { getCurrentLocale, getLocaleData } from 'grommet/utils/Locale';
import Main from './components/Main';

import en from 'react-intl/locale-data/en';
import pl from 'react-intl/locale-data/pl';

addLocaleData([...en, ...pl]);
let messages;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      locale: 'pl'
    };

    // Hide eslint errros for a while
    const consoleError = console.error.bind(console);
    console.error = (message, ...args) => {
      if (
        typeof message === 'string' &&
            message.startsWith('[React Intl] Missing message:')
      ) {
        return;
      }
      consoleError(message, ...args);
    };
  }

  render() {
    const { locale } = this.state;

    try {
      messages = require(`./messages/${locale}`);
    } catch (e) {
      messages = require('./messages/en');
    }
    const localeData = getLocaleData(messages, locale);

    return (<IntlProvider
      locale={this.state.locale}
      messages={localeData.messages}
    >
      <Main />
    </IntlProvider>);
  }
}

export default App;
