const DESKTOP_NAVIGATION_HEIGHT = 72;
const MOBILE_NAVIGATION_HEIGHT = 72;

const THEME = {
  brand: '#5691c8',
  brandVariant: '#0055dd',
  brandLighter: '#5ca5e2'
};

export { DESKTOP_NAVIGATION_HEIGHT, MOBILE_NAVIGATION_HEIGHT, THEME}